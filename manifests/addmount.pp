define autofs::addmount (
  Array[Struct[{mountpoint => String,
		map => String,
		Optional[options] => String,
		Optional[content] => String,
		Optional[source] => String}]] $mounts,
) {
    include ::autofs

    if $facts['kernel'] == 'SunOS' {
	$mounts.each |$mount| {
	    file_line{"autofs_${title}":
		ensure => 'present',
		path   => '/etc/auto_master',
		match  => "${mount['mountpoint']} ",
		line   => sprintf("%-15s %s%s", $mount['mountpoint'], $mount['map'], ('options' in $mount)?{true => "\t${mount['options']}", false => ''},),
                notify => Service['autofs'],
	    }
	}
    } else {
	file {"/etc/auto.master.d/${title}.autofs":
	    ensure  => 'file',
	    owner   => 'root',
	    group   => 'root',
	    mode    => '0644',
	    content => epp('autofs/autofs.epp', {
		mounts => $mounts,
	    }),
	    notify  => Service['autofs'],
	}
    }

    $mounts.each |$mount| {
	if 'content' in $mount {
	    file {"${mount['map']}":
		ensure  => 'file',
		owner   => 'root',
		group   => 'root',
		mode    => '0644',
		seluser => 'system_u',
		selrole => 'object_r',
		seltype => 'bin_t',
		content => $mount['content'],
		notify  => Service['autofs'],
	    }
	} elsif 'source' in $mount {
	    file {"${mount['map']}":
		ensure  => 'file',
		owner   => 'root',
		group   => 'root',
		mode    => '0644',
		seluser => 'system_u',
		selrole => 'object_r',
		seltype => 'bin_t',
		source  => $mount['source'],
		notify  => Service['autofs'],
	    }
	}
    }
}
