class autofs (
    String $package_ensure,
    String $service_name,
    String $service_ensure,
    Boolean $service_enable,
    String $master_map,
    String $master_map_owner,
    String $master_map_group,
    String $master_map_mode,
) {
    package {'autofs': ensure => $package_ensure, }

    service {'autofs':
        name    => $service_name,
        ensure  => $service_ensure,
        enable  => $service_enable,
        restart => 'systemctl reload autofs.service',
        require => Package['autofs'],
    }

    if $facts['kernel'] == 'SunOS' {
	file {"${master_map}":
	    ensure  => 'file',
	    owner   => $master_map_owner,
	    group   => $master_map_group,
	    mode    => $master_map_mode,
            require => Package['autofs'],
	}
    } else {
	file {"${master_map}":
	    ensure  => 'file',
	    owner   => $master_map_owner,
	    group   => $master_map_group,
	    mode    => $master_map_mode,
	    content => epp('autofs/auto.master.epp'),
            require => Package['autofs'],
	}

	file { '/etc/auto.master.d':
	    ensure  => 'directory',
	    owner   => 'root',
	    group   => 'root',
	    mode    => '0755',
            require => Package['autofs'],
	}
    }
}
